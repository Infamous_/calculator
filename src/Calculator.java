abstract class Calculator {
    public abstract int add(int num1, int num2);
    public abstract int subtract(int num1, int num2);
    public abstract int multiply(int num1, int num2);
    public abstract int divide(int num1, int num2);
}
