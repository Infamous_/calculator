import java.util.Scanner;
class UserInterface {
    private Scanner scanner;

    public UserInterface() {
        scanner = new Scanner(System.in);
    }

    public int getNumberSystem() {
        System.out.println("Выберите систему счисления (ввести число):");
        System.out.println("16. HEX");
        System.out.println("10. DEC");
        System.out.println("8. OCT");
        System.out.println("2. BIN");
        if (scanner.hasNextInt()) {
            return scanner.nextInt();
        }

        return 0;
    }

    public int getOperation() {
        System.out.println("Выберите операцию:");
        System.out.println("1. Сложение");
        System.out.println("2. Вычитание");
        System.out.println("3. Умножение");
        System.out.println("4. Деление");
        if (scanner.hasNextInt()) {
            return scanner.nextInt();
        }
        return 0;
    }

    public void displayResult(int result) {
        System.out.println("Результат:");
        System.out.println("Шестнадцатеричная: " + Integer.toHexString(result).toUpperCase());
        System.out.println("Десятичная: " + result);
        System.out.println("Восьмеричная: " + Integer.toOctalString(result));
        System.out.println("Двоичная: " + Integer.toBinaryString(result));
        System.out.println();
    }
    }