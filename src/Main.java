import java.util.Scanner;
public class Main
{
    public static void main(String[] args)
    {
        Scanner scanner = new Scanner(System.in);
        UserInterface ui = new UserInterface();
        int operation = 0;
        Calculator calculator = null;
            while (true) {
                int numberSystem;
                try {
                    numberSystem = ui.getNumberSystem();
                } catch (Exception e) {
                    System.out.println("Ошибка при выборе системы счисления: " + e.getMessage());
                    continue;
                }
                try {
                    if (numberSystem == 16) {
                        calculator = new HexCalculator();
                    } else if (numberSystem == 10) {
                        calculator = new DecimalCalculator();
                    } else if (numberSystem == 8) {
                        calculator = new OctCalculator();
                    } else if (numberSystem == 2) {
                        calculator = new BinaryCalculator();
                    } else {
                        System.out.println("Неверный выбор системы счисления! Начните с начала");
                        break;
                    }
                }
                catch (Exception e) {
                    System.out.println("Ошибка" + e.getMessage());
                    break;
                }

                System.out.println("Введите первое число: ");
                String input = scanner.next();
                if (input.startsWith("0") && numberSystem != 2) {
                    System.out.println("Число не должно начинаться с нуля, если не используется двоичная система счисления. Попробуйте еще раз.");
                    break;
                }
                int num1;
                try {
                    num1 = Integer.parseInt(input, numberSystem);
                } catch (NumberFormatException e) {
                    System.out.println("Неверный формат числа для выбранной системы счисления. Начните с начала.");
                    break;
                }

                System.out.println("Введите второе число: ");
                input = scanner.next();
                if (numberSystem != 2 && input.startsWith("0")) {
                    System.out.println("Число не должно начинаться с нуля, если не используется двоичная система счисления. Начните с начала.");
                    break;
                }
                int num2;
                try {
                    num2 = Integer.parseInt(input, numberSystem);
                } catch (NumberFormatException e) {
                    System.out.println("Неверный формат числа для выбранной системы счисления. Попробуйте еще раз.");
                    break;
                }
                operation = ui.getOperation();

                int result;
                try {
                    if (operation == 1) {
                        result = calculator.add(num1, num2);
                    } else if (operation == 2) {
                        result = calculator.subtract(num1, num2);
                    } else if (operation == 3) {
                        result = calculator.multiply(num1, num2);
                    } else if (operation == 4) {
                        try {
                            result = calculator.divide(num1, num2);
                        }
                        catch (ArithmeticException e) {
                            System.out.println("Ошибка: " + e.getMessage());
                            break;
                        }
                    } else {
                        System.out.println("Неверный выбор операции!");
                        break;
                    }
                    ui.displayResult(result);
                }
                catch (Exception e) {
                    System.out.println("Ошибка" + e.getMessage());
                }


            }

    }
}